}

}

}

}

ShowFnReq showFnReq = {

funcName: "helloWorld",

versionNum: 1

};

ShowFnRes|grpc:Error showFnRes = ep->show_fn(showFnReq);

if showFnRes is error {

io:println("Error retrieving fn: ", showFnRes.message());

} else {

io:println("Success: ", showFnRes);

}

ShowAllFnsReq showAllFnsReq = {

funcName: "helloWorld"

};

stream<ShowAllFnsRes, grpc:Error?>|grpc:Error showAllFnsStream = ep->show_all_fns(showAllFnsReq);

if showAllFnsStream is error {

io:println("Failed to show all fns: ", showAllFnsStream.message());

} else {

error? e = showAllFnsStream.forEach(function(ShowAllFnsRes res) {

io:println("Successfully retrieved fn: ", res.fn.name);

});

}

ShowAllWithCritReq[] showAllWithCritReqs = [{

keywords: ["testing", "beginner"],

lang: ""

}];

Show_all_with_criteriaStreamingClient|grpc:Error showAllWithCritStream = ep->show_all_with_criteria();

if showAllWithCritStream is error {

io:println("error occured setting up showAllWithCrit stream: ", showAllWithCritStream.message());

} else {

foreach ShowAllWithCritReq showAllWithCritReq in showAllWithCritReqs {

error? err = showAllWithCritStream->sendShowAllWithCritReq(showAllWithCritReq);

if err is error {

io:println("failed to send showAllWithCriteria req: ", err.message());

}

}

error? err = showAllWithCritStream->complete();

if err is error {

io:println("failed to send showAllWithCriteria complete message: ", err.message());

}

ShowAllWithCritRes|grpc:Error? showAllWithCritRes = showAllWithCritStream->receiveShowAllWithCritRes();

while true {

if showAllWithCritRes is error {

io:println("failed to send showAllWithCriteria complete message: ", showAllWithCritRes.message());

break;

} else {

if showAllWithCritRes is () {

break;

} else {

io:println("showAllWithCriteria response: ", showAllWithCritRes.fns);

showAllWithCritRes = showAllWithCritStream->receiveShowAllWithCritRes();

}

}

}

}

DeleteFnReq delFnReq = {

funcName: "helloWorld",

versionNum: 1

};

DeleteFnRes|grpc:Error delRes = ep->delete_fn(delFnReq);

if delRes is error {

io:println("Error deleting fn: ", delRes.message());

} else {

io:println("Success: ", delRes.message);